# TODO здесь писать код
import requests
import json
from pprint import pprint

uri = "https://swapi.dev/api/"
starship_search_name = "Millennium Falcon"

starship_data = json.loads(requests.get(uri+f"starships/?search={starship_search_name}").text)["results"][0]

final_data = {
    "name": starship_data["name"],
    "max_atmosphering_speed": starship_data["max_atmosphering_speed"],
    "starship_class": starship_data["starship_class"],
    "pilots": [
        {
            "name": pilot_data["name"],
            "height": pilot_data["height"],
            "mass": pilot_data["mass"],
            "homeworld_name": json.loads(requests.get(pilot_data["homeworld"]).text)["name"],
            "homeworld": pilot_data["homeworld"]
        }
        for pilot_data in [json.loads(requests.get(pilot_url).text) for pilot_url in starship_data["pilots"]]
    ]
}

pprint(final_data)
