import json


def get_difference_in_dicts(dict_1: dict, dict_2: dict):
    difference = dict()

    def get_diff(d_1, d_2):
        for key, value in d_1.items():
            if not isinstance(value, dict):
                if value != d_2[key]:
                    difference[key] = {f"first": value, f"second": d_2[key]}
            else:
                get_diff(d_1[key], d_2[key])
    get_diff(dict_1, dict_2)
    return difference


with open("json_old.json", "r") as old_json_dict:
    old_data = json.load(old_json_dict)

with open("json_new.json", "r") as new_json_dict:
    new_data = json.load(new_json_dict)

dif_dict = get_difference_in_dicts(old_data, new_data)
diff_list = ["services", "staff", "datetime"]


result = {key: dif_dict[key]["second"] for key in diff_list if key in dif_dict}
print(result)
