# TODO здесь писать код
from typing import List
import re

example_data = ['9995299999', '999999-999', '99999x9999']

def check_phone_number(num: str):
    if re.match(r"[89]\d{9}", num):
        print(num, "всё в порядке")
    else:
        print(num, "не подходит")

for i in map(check_phone_number, example_data):
   pass