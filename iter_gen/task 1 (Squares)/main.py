class SquaresIterClass:
    """
        Класс итератор последовательности из квадратов чисел от 1 до last  (1 ** 2, 2 ** 2, 3 ** 2 и так далее)
        :param last: int
    """
    def __init__(self, last: int, first: int = 1):
        self.first = first
        self.last = last

    def __iter__(self):
        return self

    def __next__(self) -> int:
        if self.first <= self.last:
            iter_square = self.first ** 2
            self.first += 1
            return iter_square
        else:
            raise StopIteration


def squares_generator_function(last: int, first: int = 1):
    """
        Функция генератор последовательности из квадратов чисел от 1 до last  (1 ** 2, 2 ** 2, 3 ** 2 и так далее)
        :param first:
        :param last: int
    """
    while first <= last:
        yield first ** 2
        first += 1


def get_a_squares_generator(last: int, first: int = 1):
    return (i ** 2 for i in range(first, last + 1))


if __name__ == '__main__':
    N = int(input("Введите целое число\n"))
    # N = 4

    out_iter = []
    squares = SquaresIterClass(N)
    for square in squares:
        out_iter.append(square)
    print(f"Последовательность SquaresIterClass:             {out_iter}")

    out_gen_func = []
    squares = squares_generator_function(N)
    for square in squares:
        out_gen_func.append(square)
    print(f"Последовательность squares_generator_function:   {out_gen_func}")

    squares = squares_generator_function(N)
    out_gen = []
    for square in squares:
        out_gen.append(square)
    print(f"Последовательность squares_generator_expression: {out_gen}")
