import os


def gen_files_path(catalog_name: str = os.path.abspath(".").split("/")[-1], directory: str = os.path.abspath(os.path.join(os.path.sep))):
    """
    Находит указанный пользователем каталог и генерирует пути всех встреченных файлов.
    :param catalog_name: Название каталога (по умолчанию — название текущего каталога)
    :param directory: Стартовая директория (по умолчанию — корневой диск)
    :return: Возвращает объект генератора
    """
    for path, _, files in os.walk(directory):
        if catalog_name == path.split("/")[-1]:
            for file_name in files:
                yield os.path.join(path, file_name)


if __name__ == '__main__':
    input_catalog_name = input()
    for file_path in gen_files_path(input_catalog_name, directory="/home"):
        print(file_path)
