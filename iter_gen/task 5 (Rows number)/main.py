import os


def counter_code_rows_in_python_files_in_dir(dir_path: str = "."):
    """
    Функция-генератор, которая берёт все питоновские файлы в директории и вычисляет общее количество строк кода, игнорируя пустые строки и строчки комментариев.
    :param dir_path: директория
    :return: генератор
    """
    python_files = []
    for root, dirs, files in os.walk(dir_path):
        for file in files:
            if file.endswith('.py'):
                python_files.append(root + '/' + str(file))
    for python_file in python_files:
        code_rows = 0
        file1 = open(python_file, "r")
        for row in file1:
            row = row.strip()  # Удаляем лишние пробелы
            if not (
                    row == "\n"
                    or row == ""
                    or row.startswith("#")
            ):

                code_rows += 1
        file1.close()
        yield file1.name, code_rows


if __name__ == '__main__':
    for file_name, rows_num in counter_code_rows_in_python_files_in_dir("."):
        print(file_name, rows_num)
