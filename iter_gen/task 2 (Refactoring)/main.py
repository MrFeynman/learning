from typing import List


def all_multipliers(multiplier_list_1: List[int], multiplier_list_2: List[int]):
    """
    :param multiplier_list_1: первая группа множителей
    :param multiplier_list_2: вторая группа множителей
    :return: объект, который создаёт последовательность попарного произведения множителей первой и второй групп
    """
    for i in multiplier_list_1:
        for j in multiplier_list_2:
            print(i, j, i * j)
            yield i * j


if __name__ == '__main__':
    list_1 = [2, 5, 7, 10]
    list_2 = [3, 8, 4, 9]
    to_find = 56

    if to_find in all_multipliers(list_1, list_2):
        print("Found!!!")
