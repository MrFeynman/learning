from typing import Any, Iterable


class _Container:
    def __init__(self, obj: Any = None) -> None:
        """
        :param obj: Объект
        next_obj: Указатель на следующий объект
        """
        self.obj = obj
        self.next_obj = None


class LinkedList:
    """
    Связный список. Узлы реализованы классом _Container
    """
    def __init__(self, iterable: Iterable = ()):
        self.head = None
        for i in iterable:
            self.append(i)

    def _get_container_by_index(self, index: int) -> _Container:
        """
        Получение указателя на элемент по индексу.
        :param index: Индекс элемента.
        :return: Указатель на элемент.
        """
        if index < 0:
            if abs(index) > self.__len__():
                raise IndexError("Out of range")
            index = self.__len__() + index
        elif index >= self.__len__():
            raise IndexError("Out of range")
        index_container = self.head
        for i in range(index):
            index_container = index_container.next_obj
        return index_container

    def append(self, obj: Any) -> None:
        """
        Добавление элемента в конец списка.
        :param obj: Объект
        :return: None
        """
        new_container = _Container(obj=obj)
        if self.head is None:
            self.head = new_container
            return
        last_container = self.head
        while last_container.next_obj:
            last_container = last_container.next_obj
        last_container.next_obj = new_container

    def get(self, index: int) -> Any:
        """
        Получение элемента по индексу.
        :param index: Индекс элемента
        :return: None
        """
        index_container = self._get_container_by_index(index)
        return index_container.obj

    def remove(self, index: int) -> None:
        """
        Удаление элемента по индексу.
        :param index: Индекс элемента
        :return: None
        """
        removing_container = self._get_container_by_index(index)
        if self.__len__() == 1:
            self.head = None
            return
        if self.head == removing_container:
            self.head = removing_container.next_obj
            return
        previous_removing_container = self._get_container_by_index(index - 1)
        if removing_container.next_obj is None:
            previous_removing_container.next_obj = None
        else:
            previous_removing_container.next_obj = removing_container.next_obj

    def __iter__(self):
        if self.head is None:
            return
        last_container = self.head
        while last_container.next_obj:
            yield last_container.obj
            last_container = last_container.next_obj
        yield last_container.obj

    def __str__(self):
        return f"{[i for i in self]}"

    def __repr__(self):
        return self.__str__()

    def __len__(self) -> int:
        last_container = self.head
        if last_container is None:
            return 0
        else:
            len = 1
            while last_container.next_obj:
                len += 1
                last_container = last_container.next_obj
            return len


if __name__ == '__main__':
    my_list = LinkedList()
    my_list.append(10)
    my_list.append(20)
    my_list.append(30)
    print('Текущий список:', my_list)
    print('Получение третьего элемента:', my_list.get(2))
    print('Удаление второго элемента.')
    my_list.remove(1)
    print('Новый список:', my_list)
