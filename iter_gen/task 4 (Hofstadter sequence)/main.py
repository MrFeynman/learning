from typing import List, Optional


class QHofstadter:
    def __init__(self, configuration: List[int]):
        """
        :param configuration: Список первых n членов последовательности Q Хофштедтера (данный класс-итератор подразумевает точную последовательность Хофштадтера)
        Класс - итератор последовательности Хофштедтера
        """
        self.configuration = configuration
        self.i = -1
        self.amount = None

    def __call__(self, amount: Optional[int] = None):
        """

        :param amount: Количество генерируемых членов последовательности, включая начальные.
        :return: Итератор последовательности в количестве amount
        """
        self.amount = amount
        return self.__iter__()

    def __getitem__(self, item):
        """
        :param item: Номер Q-числа в последовательности.
        :return: Q-число последовательности, стоящее на месте item.
        """
        if len(self.configuration) >= item:
            return self.configuration[item - 1]
        else:
            for _ in range(item):
                next(self)
            return self.configuration[-1]

    def __iter__(self):
        """
        :return: Итератор последовательности до бесконечности
        """
        self.i = -1
        return self

    def __next__(self):
        """
        :return: Следующее Q-число последовательности
        """
        self.i += 1
        if self.amount is not None:
            if self.amount == self.i:
                raise StopIteration
        if self.configuration == [1, 2]:
            raise StopIteration
        if len(self.configuration) - 1 >= self.i:
            return self.configuration[self.i]
        else:
            try:
                self.configuration.append(self.configuration[-self.configuration[-1]] + self.configuration[-self.configuration[-2]])
                return self.configuration[-1]
            except IndexError:
                raise StopIteration


if __name__ == '__main__':
    QH_1_1 = QHofstadter([1, 1])
    for i in QH_1_1(20):
        print(i)
