import aiohttp
import asyncio
import time
from pprint import pprint

start_time = time.time()

urls = [
    "http://localhost:8000/products/get_all?page=1&limit=10",
    "http://localhost:8000/products/get_product?product_id_id=42"
]

async def main():

    async with aiohttp.ClientSession() as session:

        for url in urls:
            async with session.get(url) as resp:
                response = await resp.json()
                pprint(response)

asyncio.run(main())
print("--- %s seconds ---" % (time.time() - start_time))