import socket

sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM) # IPv4, UDP
sock.bind(('', 8888)) #reserve 8888 port on localhost machine
result = sock.recv(1024) #wait for 1024 bit message frome some client
print('Message', result.decode('utf-8')) #print decoded message
sock.close() #let free reserved port