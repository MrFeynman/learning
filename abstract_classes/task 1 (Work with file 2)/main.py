import math
from abc import ABC, abstractmethod


class MyMath(ABC):
    """
    Class with some math methods
    """
    pi: float = math.pi

    @classmethod
    def circle_len(cls, radius: float) -> float:
        """
        Calculating the circumference
        :param radius: radius
        :return: circle len
        """
        return 2 * radius * cls.pi

    @classmethod
    def circle_sq(cls, radius: float) -> float:
        """
        Calculating the area of a circle
        :param radius: radius
        :return: area of a circle
        """
        return cls.pi * radius ** 2

    @classmethod
    def cube_vlm(cls, side: float) -> float:
        """
        Calculating the volume of a cube
        :param side: cube side
        :return: volume of a cube
        """
        return side ** 3

    @classmethod
    def sphere_sq(cls, radius: float) -> float:
        """
        Calculating the surface area of a sphere.
        :param radius: radius
        :return: surface area of a sphere
        """
        return 4 * cls.pi * radius ** 2


if __name__ == '__main__':
    res_1 = MyMath.circle_len(radius=5)
    res_2 = MyMath.circle_sq(radius=6)
    print(res_1)
    print(res_2)
