from abc import ABC, abstractmethod


class File:
    def __init__(self, filename, mode):
        """
        :param filename: File name
        :param mode:
            'r' - for read
            'w' - for write (before writing file will be clear
            'x' - for exclusive create_file (not rise any error is filename is already exist)
            'a' - for writing in the end of file
            '+' - r+w
            't' - text mode
            'b' - binary mode (for systems where binary and text modes is difficult)
        errors:
            not raise any errors
            if file is not exist create a file with name <filename> in 'w' mode
        """
        self.filename = filename
        self.mode = mode

    def __enter__(self):
        try:
            self.__file = open(self.filename, self.mode)
            print(f'File {self.filename} is opening in "{self.mode}" mode.')
        except FileNotFoundError:
            print(f'File {self.filename} created. It`s opening in "w" mode.')
            self.__file = open(self.filename, "w")
        return self.__file

    def __exit__(self, exc_type, exc_value, exc_traceback):
        print(f'Closing the file {self.filename}.')
        if not self.__file.closed:
            self.__file.close()
        return True


with File('data.txt', 'r') as f:
    print(f)
