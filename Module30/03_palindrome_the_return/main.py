# TODO здесь писать код
from collections import Counter

a = {
    "a": 1
}

def can_be_poly(string: str) -> bool:
    return len(list(filter(lambda x: x[1] % 2, Counter(string).most_common()))) < 2


print(can_be_poly('abcba'))
print(can_be_poly('dabbbcd'))