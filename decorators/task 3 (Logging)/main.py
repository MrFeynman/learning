from typing import Callable, Any
from functools import wraps
from datetime import datetime
import os


def logging(func: Callable) -> Callable:
    @wraps(func)
    def wrapper(*args, **kwargs) -> Any:
        print(func.__name__)
        print(func.__doc__)
        try:
            value = func(*args, **kwargs)
        except Exception as e:
            error_time = datetime.now()
            with open("function_errors.log", "a") as log_file:
                log_file.write(f"[{error_time.isoformat()}] function_name: {func.__name__} error: {e}\n")
            return None
        return value
    return wrapper


@logging
def nice_func():
    """
    Best function for test
    :return: None
    """
    print("i'm better")


@logging
def division_zero():
    """
    Make a devision by zero
    :return: None
    """
    return 4/0


@logging
def index_out_of_range():
    """
    Devision Get list object by index out of range
    :return: None
    """
    a = [1, 2, 3]
    return a[5]


if __name__ == '__main__':
    nice_func()
    division_zero()
    index_out_of_range()
    index_out_of_range(3)
