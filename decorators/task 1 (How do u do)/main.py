from typing import Callable, Any
from functools import wraps


def wait_for(secs: int) -> Callable:
    """
        :param secs: Количество секунд.
        :return: Декоратор, который перед выполнением декорируемой функции ждёт несколько секунд.
    """
    def decorator_for_wait(func: Callable) -> Callable:
        from time import sleep

        @wraps(func)
        def wrapper(*args, **kwargs) -> Any:
            print(f"Ждём {secs} секунд")
            sleep(secs)
            value = func(*args, **kwargs)
            return value
        return wrapper
    return decorator_for_wait


@wait_for(10)
def test():
    print('<Тут что-то происходит...>')


test()
