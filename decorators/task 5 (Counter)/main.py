from typing import Callable, Any, Optional, Dict
from functools import wraps


class Counter:
    """
        The decorator counts and outputs the number of calls to the decorated function.
        :param func: decorated function
        :return:
    """
    _func_call_count: Dict[str, int] = {}

    def __call__(self, func: Callable) -> Callable:
        @wraps(func)
        def wrapper(*args, **kwargs) -> Any:
            self._func_call_count[func.__name__] = self._func_call_count.get(func.__name__, 1)
            print(f"\n\n{func.__name__} вызывалась {self._func_call_count[func.__name__]} раз")
            self._func_call_count[func.__name__] = self._func_call_count.get(func.__name__, 1) + 1
            value = func(*args, **kwargs)
            return value
        return wrapper


counter = Counter()


@counter
def greeting(name: str, age: Optional[int] = None) -> str:
    """
    Say "hi" if age is None
    Admires age if age is transmitted
    :param name: person name (obligatory)
    :param age: person age (optional)
    :return: None
    """
    if age:
        return "Ого, {name}! Тебе уже {age} лет, ты быстро растёшь!".format(name=name, age=age)
    else:
        return "Привет, {name}!".format(name=name)


@counter
def test():
    print('<Тут что-то происходит...>')


if __name__ == '__main__':
    print(greeting("Том"))
    print(greeting("Миша", 35))
    print(greeting("Миша", age=100))
    print(greeting(name="Катя", age=16))
    test()
    test()
