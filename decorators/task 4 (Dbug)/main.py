from typing import Callable, Any, Optional
from functools import wraps
from datetime import datetime
import os


def debug(func: Callable) -> Callable:
    @wraps(func)
    def wrapper(*args, **kwargs) -> Any:
        str_args = ", ".join([repr(a) for a in args] + [k + "=" + repr(v) for k, v in kwargs.items()])
        print(f"\n\nВызывается {func.__name__}({str_args})")
        value = func(*args, **kwargs)
        print(f"'{func.__name__}' вернула значение '{value}'")
        return value
    return wrapper


@debug
def greeting(name: str, age: Optional[int] = None) -> str:
    """
    Say "hi" if age is None
    Admires age if age is transmitted
    :param name: person name (obligatory)
    :param age: person age (optional)
    :return: None
    """
    if age:
        return "Ого, {name}! Тебе уже {age} лет, ты быстро растёшь!".format(name=name, age=age)
    else:
        return "Привет, {name}!".format(name=name)


if __name__ == '__main__':
    print(greeting("Том"))
    print(greeting("Миша", 35))
    print(greeting("Миша", age=100))
    print(greeting(name="Катя", age=16))
