from typing import Callable, Any
from functools import wraps


def how_are_you(func: Callable) -> Callable:
    """
    Декоратор, который спрашивает у пользователя «Как дела?», вне зависимости от ответа отвечает что-то вроде «А у меня не очень!» и только потом запускает саму функцию.
    :param func: Декорируемая функция
    :return:
    """
    @wraps(func)
    def wrapper(*args, **kwargs) -> Any:
        input("Как дела? ")
        print("А у меня не очень! Ладно, держи свою функцию.")
        value = func(*args, **kwargs)
        return value
    return wrapper


@how_are_you
def test():
    print('<Тут что-то происходит...>')


test()
