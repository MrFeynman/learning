from functools import wraps
from typing import Callable, Any


class Application:
    router = {}
    def include_route(self, path: str, callback_func: Callable) -> None:
        self.router[path] = callback_func

    def get(self, path: str) -> Callable:
        callback_func = self.router.get(path)
        return callback_func

app = Application()

def callback(path: str) -> Callable:
    def decorate(func: Callable) -> Callable:
        @wraps(func)
        def wrapper(*args, **kwargs):
            res = func(*args, **kwargs)
            return res
        app.include_route(path, wrapper)
        return wrapper
    return decorate

@callback('//')
def example():
    print('Пример функции, которая возвращает ответ сервера')
    return 'OK'

@callback('//go')
def example():
    print('Go function')
    return 'OK'

route = app.get('//')

if route:
    response = route()
    print('Ответ:', response)
else:
    print('Такого пути нет')
