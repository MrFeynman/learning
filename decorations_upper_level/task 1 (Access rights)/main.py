from typing import Callable, Any, Optional
from functools import wraps

user_permissions = ['admin']
def check_permission(permission: str = "unknown") -> Callable:
    def decorate(func: Callable) -> Callable:
        @wraps(func)
        def wrapped(*args, **kwargs) -> Any:
            if permission in user_permissions:
                value = func(*args, **kwargs)
                return value
            else:
                print(f"PermissionError: у пользователя недостаточно прав, чтобы выполнить функцию {func.__name__}")
        return wrapped
    return decorate

@check_permission('admin')
def delete_site():
    print('Удаляем сайт')

@check_permission('user_1')
def add_comment():
    print('Добавляем комментарий')

delete_site()
add_comment()