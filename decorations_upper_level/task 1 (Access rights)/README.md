# Задача 1. Права доступа

## Что нужно сделать

Перед вами стоит задача создать и поддерживать специализированный форум. Вы только приступили и сейчас работаете над действиями, которые могут совершать посетители форума. Для разных пользователей прописаны разные возможности.

Напишите декоратор check_permission, который проверяет, есть ли у пользователя доступ к вызываемой функции, и если нет, то выдаёт исключение PermissionError.

Пример кода:

```python
user_permissions = ['admin']

@check_permission('admin')
def delete_site():
    print('Удаляем сайт')

@check_permission('user_1')
def add_comment():
    print('Добавляем комментарий')

delete_site()
add_comment()
```

## Результат:

- Удаляем сайт
- PermissionError: у пользователя недостаточно прав, чтобы выполнить функцию add_comment

## Что оценивается в задаче

 - Результат вычислений корректен.
 - Формат вывода соответствует примеру.
 - Переменные, функции и собственные методы классов имеют значащие имена (не a, b, c, d).
 - Классы и методы/функции имеют прописанную документацию.
 - Есть аннотация типов для методов/функций и их аргументов, кроме args и kwargs. Если функция/метод ничего не возвращает, то используется None.
