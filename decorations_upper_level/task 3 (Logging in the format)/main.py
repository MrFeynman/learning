import time
from functools import wraps
from typing import Callable, Any
from datetime import datetime

def log_methods(format_data: str = "%b %d %Y - %H:%M:%S"):
    def decorator(cls):
        def method_decorator(func: Callable) -> Callable:
            @wraps(func)
            def wrapper(*args, **kwargs):
                start_t = time.time()
                print(f"Запускается '{'.'.join([cls.__name__, func.__name__])}'. Дата и врмя запуска: {datetime.fromtimestamp(start_t).strftime(format_data)}")
                result = func(*args, **kwargs)
                end_t = time.time()
                print(f"Завершение '{'.'.join([cls.__name__, func.__name__])}', время работы = {round(end_t - start_t, 3)} s.")
                return result
            return wrapper
        for func_n in filter(lambda func_name: not func_name.startswith('__') and callable(getattr(cls, func_name)), dir(cls)):
            setattr(cls, func_n, method_decorator(getattr(cls, func_n)))
        return cls
    return decorator


@log_methods("%b %d %Y — %H:%M:%S")
class A:
    def test_sum_1(self) -> int:
        print('Тут метод test_sum_1.')
        number = 100
        result = 0
        for _ in range(number + 1):
            result += sum([i_num ** 2 for i_num in range(10000)])

        return result

@log_methods("%b %d %Y - %H:%M:%S")
class B(A):
    def test_sum_1(self):
        super().test_sum_1()
        print("Тут метод test_sum_1 у наследника.")


    def test_sum_2(self):
        print("Тут метод test_sum_2 у наследника.")
        number = 200
        result = 0
        for _ in range(number + 1):
            result += sum([i_num ** 2 for i_num in range(10000)])

        return result

my_obj = B()
my_obj.test_sum_1()
my_obj.test_sum_2()