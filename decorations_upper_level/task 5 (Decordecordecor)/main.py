from functools import wraps
from typing import Any, Callable


def a():
    gg = 1
    print("from a", globals())
    print(locals())
    def b():
        hh = 2
        print("from b", globals())
        print(locals())

a()

def decorator_with_args_for_any_decorator(func: Callable):
    @wraps(func)
    def wrapped(*args, **kwargs) -> Any:
        return func
    return wrapped


@decorator_with_args_for_any_decorator
def decorated_decorator(func: Callable, *gg, **ff):
    @wraps(func)
    def wrapped(*args, **kwargs) -> Any:
        print("Переданные арги и кварги в декоратор:", gg, ff)
        result = func(*args, **kwargs)
        return result
    return wrapped


@decorated_decorator(100, 'рублей', 200, 'друзей')
def decorated_function(text: str, num: int) -> None:
    print("Привет", text, num)


decorated_function("Юзер", 101)