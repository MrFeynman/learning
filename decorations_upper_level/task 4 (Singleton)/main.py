from functools import wraps
from typing import Callable, Any

def singleton(cls):
    @wraps(cls)
    def wrapped(*args, **kwargs):
        if not wrapped.instance:
            wrapped.instance = cls(*args, **kwargs)
        return wrapped.instance
    wrapped.instance = None
    return wrapped

@singleton
class Example:
    pass



print(type(Example))

my_obj = Example()
my_another_obj = Example()
my_another_obj_two = Example()

print(id(my_obj))
print(id(my_another_obj))
print(id(my_another_obj_two))

print(my_obj is my_another_obj is my_another_obj_two)

def a():
    f = 1
    def b():
        h = 2
        print(globals())

a()